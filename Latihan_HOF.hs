-- 1 Exercises from chapter 9-10 of
len xs = sum(map (\x -> 1) xs)

-- map (+1) (map (+1) xs) will add each elem of list by 2
-- map f (map g xs) will work as (fog)(xs)

iter 0 f x = x
iter n f x = f (iter (n-1) f x)

sumfold xs = foldr (+) 0 xs
mapsum xs = foldr (+) 0 (map sqrt xs)
mapsumn n = mapsum [1 .. n]

-- Behave by assigning every elem of list into its own list
-- concat every elem list into one list, creating the same list as xs
mystery xs = foldr (++) [] (map sing xs)
                where sing x = [x]

-- 2 List Comprehensions
addmap xs = map (+1) xs

pair xs ys = [ x+y | x <- xs, y <-ys ]
pairmap xs ys = concat(map (\x -> map (\y -> x+y) ys) xs)

add2 xs = [ x+2 | x <- xs, x > 3 ]
add2map xs = map (+2) (filter (>3) xs)

add3x xys = [ x+3 | (x,_) <- xys ]

first (x,y) = x
add3xmap xys = map (+3) (map first xys)
