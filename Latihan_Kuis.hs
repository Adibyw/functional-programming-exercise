pyhtagoras = [ (x,y,z) | z <- [5 .. ], y <- [z-1,z-2 .. 1], x <- [y-1,y-2 .. 1], (x * x) + (y * y) == z*z]

fibonacci = fibo (1,1) 
            where fibo (x,y) = x:fibo(y,x+y)

kpk x y = head [ z | z <- [x,x*2 .. ], z `mod` y == 0 ]

fpb x y = head [ z | z <- [(min x y), (min x y)-1 .. 1], x `mod` z == 0, y `mod` z == 0 ]

myflip f a b = f b a
func n a b = n * (a + b)

for :: [a] -> (a -> b) -> [b]
for xs f = map (\x -> f x) xs

toOne x = 1
